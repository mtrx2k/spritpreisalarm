function saveCookie(object){
  document.cookie = JSON.stringify(object);
}

function loadCookie(){
  return JSON.parse(document.cookie);
}

function clearCookie(){
  document.cookie = "";
}

function appendCity(lon, lat, name){
  try{
    obj = loadCookie();
  } catch {
    obj = Object();
    obj.cities = [];
  }
  obj["cities"].push([lon, lat, name]);
  saveCookie(obj);
}
