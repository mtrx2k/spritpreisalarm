console.log("WebServices Projekt von TIM18 - Spritpreise");
console.log("Grupppe: Thong, Joey, Leon, Stefan");

var xhttp = new XMLHttpRequest();
var icons = [];

//prices
var best = 99999.0;
var worst = 1.0;
var type = "e5"

var resetCenter = true;

//Global variables
var data = null;
var map = null;
var geolocation = null;
var requestable = true;

var localStations = [];

//input
var positionInput = document.getElementsByClassName("posInput")[0];
positionInput.addEventListener("keyup", function(event){
  searchLocation(event);
});

//Radius
var comparisionRadius = 0.25;
var renderRadius = 0.1;

async function onLoad(lng, lat) {
  await downloadData(lng, lat);
}

function searchLocation(event){
  if (event == null){
    geocode();
  }
  else if (event.keyCode === 13){
    geocode();
  }
}

function geocode(){
  console.log("searching for " + positionInput.value);
  if (positionInput.value != ""){
    for (var i = 0; i < data["stations"].length; i++){
      try{
        if (positionInput.value.toLowerCase() == data["stations"][i]["place"].toLowerCase()){
          map.getView().setCenter(ol.proj.transform([data["stations"][i]["lng"], data["stations"][i]["lat"]], 'EPSG:4326', 'EPSG:3857'))
          return;
        }
        else if (positionInput.value.toLowerCase() == localStations[i]["brand"].toLowerCase()){
          map.getView().setCenter(ol.proj.transform([localStations[i]["lng"], localStations[i]["lat"]], 'EPSG:4326', 'EPSG:3857'))
          return;
        }
      } catch {

      }
    }
  }
  alert("Ort / Tankstelle nicht gefunden");
}

xhttp.onreadystatechange = function(){
  if (this.readyState == 4 && this.status == 200) {
    var tmpData = JSON.parse(this.responseText);
    if (data==null){
      data=tmpData;
    }
    else
    {
      for (i=0; i < tmpData["stations"].length; i++){
        if (!(isInDB(tmpData["stations"][i]["id"]))){
          if (tmpData["stations"][i] != null){
            data["stations"].push(tmpData["stations"][i]);
          }
        }
      }
    }
    createMap();
    createPrices(type);
    createMarkers();
  }
};

async function downloadData(lng, lat){
  xhttp.open("GET", "data.txt?" + lng + "&" + lat, true);
  await xhttp.send();
}

function createMarkers(){
  var mp = map;
  var point = new ol.geom.Point(mp.getView().getCenter()[0], mp.getView().getCenter()[1]);
  var coord = [mp.getView().getCenter()[0], mp.getView().getCenter()[1]];
  var rcoord = ol.proj.transform(coord, 'EPSG:900913', 'EPSG:4326');
  removeAllOverlays();
  for (i=0; i < data["stations"].length; i++){
    if (data["stations"][i]["lng"] - rcoord[0] > -renderRadius && data["stations"][i]["lng"] - rcoord[0] < renderRadius){
      if (data["stations"][i]["lat"] - rcoord[1] > -renderRadius && data["stations"][i]["lat"] - rcoord[1] < renderRadius){
        var pos = [data["stations"][i]["lng"], data["stations"][i]["lat"]];
        var marker = gasMarker(i, pos, data["stations"][i]);
        map.addOverlay(marker[0]);
      }
    }
  }
}

function getGasData(){
  var e = document.getElementsByClassName("gasType")[0];
  type = e.options[e.selectedIndex].value;
  createPrices(type);
  createMarkers();
}

function removeAllOverlays(){
  map.getOverlays().getArray().slice(0).forEach(function(overlay) {
  if (overlay.id != "user"){
    map.removeOverlay(overlay);
  }
  });
}

function isInDB(id){
  for (i=0; i < data["stations"].length;i++){
    if (data["stations"][i]["id"] == id){
      return true;
    }
  }
  return false;
}

function createPrices(type){
  best = 99999.0;
  worst = 0.0;
  var mp = map;
  var point = new ol.geom.Point(mp.getView().getCenter()[0], mp.getView().getCenter()[1]);
  var coord = [mp.getView().getCenter()[0], mp.getView().getCenter()[1]];
  var rcoord = ol.proj.transform(coord, 'EPSG:900913', 'EPSG:4326');
  localStations = [];
  for (i=0; i < data["stations"].length; i++){
    if (data["stations"][i]["lng"] - rcoord[0] > -comparisionRadius && data["stations"][i]["lng"] - rcoord[0] < comparisionRadius){
      if (data["stations"][i]["lat"] - rcoord[1] > -comparisionRadius && data["stations"][i]["lat"] - rcoord[1] < comparisionRadius){
        if (data["stations"][i][type] != null){
          localStations.push(data["stations"][i]);
          if (data["stations"][i][type] > worst){
            worst = data["stations"][i][type];
          }
          else if (data["stations"][i][type] < best){
            best = data["stations"][i][type];
          }
        }
      }
    }
  }
}

function createMap(lonLat){
  if (map==null){
    var el = document.getElementById('maps');
    el.style.opacity = 0;
    map = new ol.Map({
      target: "maps",
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([9.452, 47.657]),
        zoom: 16
      })
    });

    geolocation = new ol.Geolocation({
        trackingOptions: {
          enableHighAccuracy: true
        },
        projection: map.getView().getProjection()
      });
    geolocation.setTracking(true);
    geolocation.on('change', function(){
      removeUserMarker();
      var uMarker = userMarker(geolocation.getPosition());
      map.addOverlay(uMarker);
      if (resetCenter){
        map.getView().setCenter(geolocation.getPosition());
        var coord = convertLocation(geolocation.getPosition());
        onLoad(coord[0], coord[1]);
        resetCenter = false;
      }
    });
    //map.baseLayer.projection = new ol.Projection("EPSG:4326");
    //lat=47.657&lng=9.452
    map.on("moveend", function(){
      onMoveEnd(map);
    });
    el.style.opacity = 1;
  }
}

function convertLocation(position){
  var mp = map;
  var point = new ol.geom.Point(position[0], position[1]);
  var coord = [position[0], position[1]];
  var rcoord = ol.proj.transform(coord, 'EPSG:900913', 'EPSG:4326');
  return rcoord;
}

function removeUserMarker(){
  map.getOverlays().getArray().slice(0).forEach(function(overlay) {
  if (overlay.id == "user"){
    map.removeOverlay(overlay);
  }
  });
}

function userMarker(position){
  var element = document.createElement('div');
  element.id = "user";
  element.classList.add("user");
  var rcoord = convertLocation(position);
  var marker = new ol.Overlay({
    position: ol.proj.fromLonLat(rcoord),
    id: "user",
    positioning: 'center-center',
    element: element,
  });
  return marker;
}

function gasMarker(id, pos, data){
  var element = document.createElement('div');
  var elementText = document.createElement('a');
  var elementInfo = document.createElement('a');
  elementText.classList.add("gasText");
  elementText.innerHTML = data["brand"];
  try{
    elementInfo.innerHTML = data[type].toString().substring(0,4) + "<a class='upperprice'>" + data[type].toString().substring(4,5) + "</a>" + "<a class='branda'>\n" + data["brand"] + "</a>";
  }catch {
    elementInfo.innerHTML = data[type];
  }
  elementInfo.classList.add("gasText");
  element.id = id;
  element.classList.add("marker");
  element = addPriceComparison(element, data[type]);
  //element.innerHTML = '<img src="/img/gas.png" id="markerimg"></img>'
  element.innerHTML = '<div id="markerimg"><a>' + elementInfo.innerHTML + '</a></div>'
  var marker = new ol.Overlay({
    position: ol.proj.fromLonLat(pos),
    positioning: 'bottom-right',
    id: id,
    element: element,
    stopEvent: true
  });
  element.onclick = function(evt){
    loadGasData(marker.id);
  };
  var point = [marker]
  return point;
}

async function onMoveEnd(evt){
  if (requestable){
    var mp = evt;
    var coord = [mp.getView().getCenter()[0], mp.getView().getCenter()[1]];
    var rcoord = convertLocation(coord);
    requestData(rcoord[0], rcoord[1]);
    requestable = false;
  }
  else{
    awaitUpdate();
  }
  await createMarkers();
}

async function awaitUpdate(){
  if (!requestable){
    setTimeout(function(){
      requestable = true;
    }, 5000);
  }
}

function addPriceComparison(element, price){
  if (price > best*1.05){
    element.classList.add("rmarker");
  }
  else if(price > best*1.03){
    element.classList.add("omarker")
  }else{

    element.classList.add("gmarker");
  }
  return element;
}

function loadGasData(id){
  var headers = "<tr><th>Super (E5)</th><th>Super (E10)</th><th>Diesel</th></tr>"
  var e5 = data["stations"][id]["e5"];
  var e10 = data["stations"][id]["e10"];
  var diesel = data["stations"][id]["diesel"];
  if (e5==null){
    e5="n.a."
  }
  if (e10==null){
    e10="n.a."
  }
  if (diesel==null){
    diesel="n.a."
  }
  var info = "<tr><td>"+ e5 + "</td><td>" + e10 + "</td><td>" + diesel + "</td></tr>";
  document.getElementById("list").innerHTML = "";
  document.getElementById("list").innerHTML  += "<tr>" + headers + info + "</tr>";
  gasHeader = document.getElementById("gasHeader");
  open = "Geschlossen...";
  if (data["stations"][id]["isOpen"]){
    open = "Ge&oumlffnet!"
    gasHeader.classList.add("open");
  }
  else{
    gasHeader.classList.add("closed");
  }
  gasHeader.innerHTML = open;
}

function requestData(lon, lat){
  if (!this.requestable){
    this.requestable = true;
    awaitUpdate();
    return;
  }
  this.requestable = false;
  if  (true){
    onLoad(lon,lat);
  }
}

function closeButton(){
  console.log("closing")
  var element = document.getElementById("popUp");
  element.style.opacity = 0;
  element.style.pointerEvents = "none";
  var table = document.getElementById("list").innerHTML = "";
}
