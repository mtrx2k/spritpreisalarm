import json
from Output import *

def createEntry(entry):
    data = loadEntrys()
    with open("./data/data.txt", "w+") as json_file:
        if not checkDB(entry, data):
            try:
                data["stations"].append(entry)
            except:
                writeWarning("Could not add to data.")
        try:
            json.dump(data, json_file)
        except:
            writeWarning("Failed dumping data.")

def createEntrys(entrys):
    data = loadEntrys()
    for entry in entrys:
        if not checkDB(entry, data):
            try:
                data["stations"].append(entry)
            except:
                writeWarning("Could not add to data.")
        else:
            try:
                data = updateEntry(entry, data)
            except:
                writeWarning("Failed updating data.")
    with open("./data/data.txt", "w+") as json_file:
        try:
            json.dump(data, json_file)
        except:
            writeWarning("Failed dumping data.")

def updateEntry(entry, data):
    for i in range(len(data)):
        if data["stations"][i]["id"] == entry["id"]:
            data["stations"][i] = entry
            break
    return data


def writeDirect(info):
    with open("./data/data.txt", "w+") as json_file:
        json.dump(info, json_file)

def loadEntrys():
    data=[]
    with open("./data/data.txt", "r+") as json_file:
        try:
            data=json.load(json_file)
        except Exception as e:
            writeWarning("Could not load DB!")
            writeWarning("%s" %str(e))
        except:
            writeWarning("Could not load DB!")
    return data

def checkDB(entry, data):
    for _entry in data["stations"]:
        if entry["id"] == _entry["id"]:
            return True
    return False

def resetEntrys():
    writeDebug("resetting DB")
    with open("./data/data.txt", "w+") as json_file:
        json.dump([], json_file)
