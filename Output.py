#Für outputs in der Konsole
import datetime
import os
from termcolor import *
import colorama

colorama.init()

def writeInfo(text):
    txt = ("[INFO][%s]: %s" %(datetime.datetime.now().strftime("%Y.%m.%d | %H:%M:%S"), text))
    print(txt)

def writeWarning(text):
    wrng = colored("WARNING", "yellow", attrs=["bold"])
    txt = ("[INFO][%s][%s]: %s" %(wrng, datetime.datetime.now().strftime("%Y.%m.%d | %H:%M:%S"), text))
    print(txt)

def writeSpacer(text):
    txt = ("==========================[%s]==========================" %text)
    print(txt)

def writeDebug(text):
    dbg = colored("DEBUG", "magenta", attrs=["bold"])
    txt = ("[INFO][%s][%s]: %s" %(dbg, datetime.datetime.now().strftime("%Y.%m.%d | %H:%M:%S"), text))
    print(txt)
