from Output import *
import requests
import JSON
import json
from Object import *

def request(lat, lng):
    writeInfo("Requesting Data.")
    try:
        response = requests.get("https://creativecommons.tankerkoenig.de/json/list.php?lat=%s&lng=%s&rad=25&sort=dist&type=all&apikey=6dd7b86a-63ad-472b-c2a1-a5f93b5a0352" %(str(lat), str(lng)))
        writeInfo("Data recieved.")
    except:
        writeWarning("Failed recieving Data. Exiting.")
        writeWarning(response.text)
        return
    writeInfo("Writing Data to DB")
    writeInfo("recieved %i stations" %len(json.loads(response.text)["stations"]))
    try:
        JSON.createEntrys(json.loads(response.text)["stations"])
    except:
        writeWarning("Could not create Entry!")
    writeInfo("Finished")
    return filterToPos(lat, lng)

def startRequest(lng, lat):
        writeInfo("Requesting Data.")
        try:
            response = requests.get("https://creativecommons.tankerkoenig.de/json/list.php?lat=%s&lng=%s&rad=25&sort=dist&type=all&apikey=6dd7b86a-63ad-472b-c2a1-a5f93b5a0352" %(str(lat), str(lng)))
            writeInfo("Data recieved.")
        except:
            writeWarning("Failed recieving Data. Exiting.")
            writeWarning(json.loads(response.text))
            return
        writeInfo("Writing Data to DB")
        writeInfo("recieved %i stations" %len(json.loads(response.text)["stations"]))
        JSON.writeDirect(json.loads(response.text))
        writeInfo("Finished")

def filterToPos(lat, lng):
    lat = float(lat)
    lng = float(lng)
    retData = Object()
    retData.stations = []
    retData = retData.__dict__
    data = JSON.loadEntrys()
    for entry in data["stations"]:
        if float(entry["lat"]) - lat > -0.3 and float(entry["lat"]) - lat < 0.3:
            if float(entry["lng"]) - lng > -0.3 and float(entry["lng"]) - lng < 0.3:
                retData["stations"].append(entry)
    writeDebug("Sending %i Stations" %len(retData["stations"]))
    return json.dumps(retData)

    """
    if (data["stations"][i]["lng"] - rcoord[0] > -renderRadius && data["stations"][i]["lng"] - rcoord[0] < renderRadius){
  if (data["stations"][i]["lat"] - rcoord[1] > -renderRadius && data["stations"][i]["lat"] - rcoord[1] < renderRadius){
    """
