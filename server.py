from http.server import BaseHTTPRequestHandler, HTTPServer
import time
from Output import *
import requests
import json
import sys, os, socket
from socketserver import ThreadingMixIn
import dataRequestHandler
import ssl

hostName = "localhost"
hostPort = 9000

#dataRequestHandler.startRequest(9.452, 47.657)

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        reqFile = self.path
        if reqFile == "/": reqFile = "/index.html"
        elif reqFile.split('?')[0] == "/data.txt":
            self.sendData(reqFile.split('?')[1])
            return
        elif reqFile == "/style.css":
            self.sendStyle()
            return
        elif reqFile.split('.')[-1] == "png":
            self.sendPicture(reqFile)
        elif reqFile.split('.')[-1] == "ico":
            self.sendIco(reqFile)
        elif reqFile.split('.')[-1] == "gif":
            self.sendGif(reqFile)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        reqFile = "./Site" + reqFile
        try:
            writeDebug("Sending File %s." %reqFile)
            self.wfile.write(bytes(open(reqFile, "r+").read(), "UTF-8"))
        except:
            writeWarning("404 - File not found: %s" %reqFile)

    def sendData(self, data):
        self.send_header("Content-type", "application/json")
        self.end_headers()
        try:
            self.wfile.write(bytes(dataRequestHandler.request(str(data.split('&')[1]), str(data.split('&')[0])), "UTF-8"))
            writeDebug("Data Sent.");
        except:
            writeWarning("Limit exceeded!");
            writeWarning("Sending last request");
            self.wfile.write(bytes(json.dumps(json.load(open("./data/data.txt", "r+"))), "UTF-8"))

    def sendStyle(self):
        self.send_header("Content-type", "text/css")
        self.end_headers()
        self.wfile.write(bytes(open("./Site/style.css", "r+").read(), "UTF-8"))

    def sendIco(self, reqFile):
        reqFile = "./Site" + reqFile
        self.send_header("Content-type", "image/x-icon")
        self.end_headers()
        self.wfile.write(open(reqFile, "rb").read())

    def sendGif(self, reqFile):
        reqFile = "./Site" + reqFile
        self.send_header("Content-type", "picture/gif")
        self.end_headers()
        self.wfile.write(open(reqFile, "rb").read())

    def sendPicture(self, reqFile):
        reqFile = "./Site" + reqFile
        self.send_header("Content-type", "picture/png")
        self.end_headers()
        self.wfile.write(open(reqFile, "rb").read())

class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass

def getInfos():
    info = open("config.cfg", "r+").read()
    infoS = info.split('\n')
    dt = []
    for inf in infoS:
        try:
            dt.append(inf.split(' ')[1])
        except:
            pass
    return dt

infos = getInfos()

#myServer = HTTPServer((infos[0], int(infos[1])), MyServer)
writeInfo("Server Starts - %s:%s" %(infos[0], int(infos[1])))

myServer = ThreadingSimpleServer((infos[0], int(infos[1])), MyServer)
myServer.socket = ssl.wrap_socket(myServer.socket,
                                server_side=True,
                                certfile='server.pem',
                                ssl_version=ssl.PROTOCOL_TLSv1)





try:
    myServer.serve_forever()
except KeyboardInterrupt:
    writeInfo("Stopping server..")

myServer.server_close()
writeInfo("Server Stops - %s:%s" %(hostName, hostPort))
